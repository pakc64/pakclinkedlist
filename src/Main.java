import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        PakcLinkedList list = new PakcLinkedList("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        System.out.println("");
        System.out.println(list.toString());
        System.out.println("Длинна списка =" + list.size());
        list.remove(3);

        System.out.println("Элементы списка после удаления:");
        for(int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
    }
}
