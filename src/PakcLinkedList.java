

public class PakcLinkedList {

    private String value;
    private int index;
    private PakcLinkedList next;

    public PakcLinkedList(String value) {
        setValue(value);
        setIndex(0);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private int getIndex() {
        return index;
    }

    private void setIndex(int index) {
        this.index = index;
    }

    public PakcLinkedList getNext() {
        return next;
    }

    public void setNext(PakcLinkedList next) {
        this.next = next;
    }

    public String get(int index) {
        if (getIndex() == index) {
            return value;
        } else if (next != null) {
            return next.get(index);
        }
        return "Такого индекса нет в массиве!";
    }

    public int size() {
        if (next != null) {
            return next.size();
        }
        return getIndex() + 1;
    }

    public void add(String nextValue) {
        if (next != null) {
            next.add(nextValue);
        } else {
            next = new PakcLinkedList(nextValue);
        }
        next.setIndex(getIndex() + 1);
    }

    public void remove(int index) {
        if (next.getIndex() == index) {
            next = next.getNext();
            next.removeIndexes();
        } else {
            next.remove(index);
        }
    }

    private void removeIndexes() {
        setIndex(getIndex() - 1);
        if (next != null) {
            next.removeIndexes();
        }
    }

    @Override
    public String toString() {
        while (next != null) {
            System.out.print(getValue() + ", ");
            return next.toString();
        }
        return value;
    }
}
